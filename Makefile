.PHONY: nim clean

nim: 
	javac nim/*.java
	java nim/NIM
	
clean:
	rm -f nim/*.class
	