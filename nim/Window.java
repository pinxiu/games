package nim;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import javax.swing.event.*;

import java.io.File;

import javax.imageio.*;

public class Window extends JComponent {
	private int WIDTH = 480, HEIGHT = 360;
	private int WSIZE = 50;
	private BufferedImage bImage;
	private boolean hasImage = false;
	private boolean hasIcon = false;
	private Color bgColor = Color.WHITE;
	private Color deadColor = Color.RED;
	private Color blockColor = Color.lightGray;
	private BufferedImage blockImage;
	private BufferedImage wordImage;
	private int blockSize = 50;
	private String bgPic = "images/default_bg_pic.png";
	private String bIcon = "images/blockIcon.png";
	private String word = "images/YourTurn.png";
	private Game game;
	private JPopupMenu popup = new JPopupMenu();
	private JFrame frame = new JFrame("NIM");
	private JTextField[] tfields;
	private JPanel tfPanel = new JPanel();
	private Control control = new Control(this);
	private JScrollPane pane = new JScrollPane(this);
	private JLabel msg = new JLabel("Choose one of the rows and specify the number of blocks you want to cross out.");
	private boolean resize = false;

	Window() {
		this.setOpaque(true);
		this.addMouseListener(new MouseHandler());
		this.addMouseMotionListener(new MouseMotionHandler());
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowHandler());
		control.setFloatable(false);
		frame.add(control, BorderLayout.NORTH);
		frame.getRootPane().setDefaultButton(control.getDefaultButton());
		frame.getContentPane().add(pane, BorderLayout.CENTER);
		tfPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		frame.getContentPane().add(tfPanel, BorderLayout.SOUTH);
		tfPanel.add(msg);
		try {
			bImage = ImageIO.read(new File(bgPic));
			hasImage = true;
		} catch (Exception e) {
			// Do nothing.
		}
		try {
			blockImage = ImageIO.read(new File(bIcon));
			hasIcon = true;
		} catch (Exception e) {
			// Do nothing.
		}
		try {
			wordImage = ImageIO.read(new File(word));
		} catch (Exception e) {
			// Do nothing.
		}
		pane.addComponentListener(new FrameListener());
		frame.pack();
		frame.setLocationByPlatform(true);
		frame.setVisible(true);
	}
	
	public boolean finished() {
		return game == null || game.gameOver();
	}

	public JFrame getFrame() {
		return frame;
	}

	public JScrollPane getPane() {
		return pane;
	}

	public Game getGame() {
		return game;
	}

	public JPopupMenu getPopup() {
		return popup;
	}

	public void setWidth(int width) {
		this.WIDTH = width;
	}

	public void setHeight(int height) {
		this.HEIGHT = height;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public void setBImage(File picture) {
		try {
			bImage = ImageIO.read(picture);
			hasImage = true;
			repaint();
		} catch (Exception e) {
			Utils.error("Invalid", "Cannot read the image.");
		}
	}

	public void setTfields(int rowNumber) {
		if (!game.happen()) {
			tfPanel.removeAll();
		//frame.getContentPane().add(tfPanel, BorderLayout.SOUTH);
			tfields = new JTextField[rowNumber];
		}
		for (int i = 0; i < rowNumber; i++) {
			if (!game.happen())
				tfields[i] = new JTextField("0", 5);
			else
				tfields[i].setText("0");
			tfields[i].addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					game.setNextMove(Window.this);
					game.turn(Window.this);
				}
			});
			if (!game.happen()) {
				tfPanel.add(new JLabel("Row " + Integer.toString(i + 1) + " :"));
				tfPanel.add(tfields[i]);
			}
		}
		frame.pack();
		repaint();
	}

	public int[] getFields(int rowNumber) {
		int[] fields = new int[rowNumber];
		int[] result = new int[2];
		boolean success = true;
		for (int i = 0; i < rowNumber; i++) {
			if (tfields[i].getText().equals("")) {
				fields[i] = 0;
			} else {
				fields[i] = Integer.parseInt(tfields[i].getText());
			}
			if (fields[i] < 0) {
				Utils.error("Invalid", "Invalid number.");
				return null;
			} else if (fields[i] > 0) {
				if (!success) {
					Utils.error("Invalid", "You can only move the blocks in one row.");
					return null;
				} else {
					result[0] = i;
					result[1] = fields[i];
					success = false;
				}
			}
		}
		if (success)
			return null;
		return result;
	}

	public void setSize() {
		resize = true;
	}

	public void setBlockSize() {
		blockSize = HEIGHT / (game.getRowNumber() + 3);
		for (int i : game.getInitFields()) {
			int rowSize = WIDTH / (i + 2);
			if (blockSize > rowSize)
				blockSize = rowSize;
		}
		if (blockSize < 10) {
			blockSize = 10;
			if (resize) {
				Utils.error("size", "Too many blocks to fit in the screen. Please resize the window or use fewer blocks.");
				resize = false;
			}
		}
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(WIDTH, HEIGHT);
	}

	@Override
	public void paintComponent(Graphics g) {
		if (hasImage)
			g.drawImage(bImage, 0, 0, WIDTH, HEIGHT, null);
		else {
			g.setColor(bgColor);
			g.fillRect(0, 0, getWidth(), getHeight());
		}
		if (game != null && game.getFields() != null) {
			int x = blockSize;
			int y = (HEIGHT - blockSize * game.getRowNumber()) / 2;
			if (game.getPlayer() == 0 && game.happen() && !game.gameOver())
				g.drawImage(wordImage, 0, 0, WIDTH, y, null);
			int[] initial = game.getInitFields();
			int count = 0;
			for (int i : game.getFields()) {
				g.setColor(blockColor);
				for (int j = 0; j < i; j++) {
					g.fillRect(x, y, blockSize - 5, blockSize - 5);
					x += blockSize;
				}
				for (int j = i; j < initial[count]; j++) {
					if (hasIcon) {
						g.fillRect(x, y, blockSize - 5, blockSize - 5);
						g.drawImage(blockImage, x, y, blockSize - 5, blockSize - 5, null);
					} else {
						g.setColor(deadColor);
						g.fillRect(x, y, blockSize - 5, blockSize - 5);
					}
					x += blockSize;
				}
				count++;
				x = blockSize;
				y += blockSize;
			}

		}
	}

	private class FrameListener implements ComponentListener {
		public void componentHidden(ComponentEvent arg0) {
        }
        public void componentMoved(ComponentEvent arg0) {   
        }
        public void componentResized(ComponentEvent arg0) {
        	//if (Window.this.resize) {
	        	Window.this.setWidth(Window.this.getPane().getWidth());
	            Window.this.setHeight(Window.this.getPane().getHeight());
	            if (game != null && game.getFields() != null)
		            Window.this.setBlockSize();
		    //}
        }
        public void componentShown(ComponentEvent arg0) {

        }
	}

	private class WindowHandler extends WindowAdapter {
		public void windowClosing(WindowEvent e) {
			if (Window.this.finished()) {
				System.exit(0);
			} else {
				Utils.error("exit", "The game is not over yet. Would you like to continue to play?",
					"Yes, continue the game.", "No, exit anyway.");
			}
		}
	}

	private class MouseMotionHandler extends MouseMotionAdapter {

		@Override
	    public void mouseDragged(MouseEvent e) {
	        //Window.this.resize = true;
	    }
	}	

	private class MouseHandler extends MouseAdapter {

		@Override
		public void mouseReleased(MouseEvent e) {
			//Window.this.resize = false;
			if (e.isPopupTrigger()) {
				showPopup(e);
			}
			e.getComponent().repaint();
		}

		@Override
		public void mousePressed(MouseEvent e) {
			//Window.this.resize = false;
			if (e.isPopupTrigger()) {
				showPopup(e);
			}
			e.getComponent().repaint();
		}

		private void showPopup(MouseEvent e) {
			popup.show(e.getComponent(), e.getX(), e.getY());
		}
	}
}