package nim;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import javax.swing.event.*;

import java.util.Stack;

public class Game {
	private int rowNumber;
	private int totalPiece;
	private int[] fields;
	private int[] initialFields;
	private int player; // 1 is Computer
	private int[] nextMove;
	private boolean happen = false;
	private Stack<int[]> history = new Stack<int[]>();

	public Game(int rowNumber) {
		this.rowNumber = rowNumber;
		this.initialFields = new int[rowNumber];
	}

	void setFields(int[] fields) {
		this.fields = fields;
		for (int i = 0; i < rowNumber; i++) {
			totalPiece += fields[i];
			initialFields[i] = fields[i];
		}
	}

	void setHappen() {
		happen = true;
	}

	boolean happen() {
		return happen;
	}

	int[] getFields() {
		return fields;
	}

	int[] getInitFields() {
		return initialFields;
	}

	int getRowNumber() {
		return rowNumber;
	}

	int getPlayer() {
		return player;
	}

	void backup() {
		int[] temp = new int[rowNumber];
		for (int i = 0; i < rowNumber; i++) {
			temp[i] = fields[i];
		}
		history.push(temp);
	}

	int sumUp(int[] x) {
		int sum = 0;
		for (int i : x)
			sum += i;
		return sum;
	}

	void undo() {
		if (!history.empty()) {
			history.pop();
		}
		if (!history.empty()) {
			fields = history.peek();
		} else {
			for (int i = 0; i < rowNumber; i++) {
				fields[i] = initialFields[i];
			}
			backup();
		}
		totalPiece = sumUp(fields);
	}

	void restart() {
		happen = false;
		history = new Stack<int[]>();
		for (int i = 0; i < rowNumber; i++) {
			fields[i] = initialFields[i];
		}
		totalPiece = sumUp(fields);
		nextMove = null;
	}

	int move(int row, int num) {
		if (num > fields[row]) {
			Utils.error("Invalid", "There are not enough blocks in the selected row.");
			return 0;
		} else {
			fields[row] -= num;
			totalPiece -= num;
			return 1;
		}
	}

	int move() {
		int total = 0;
		for (int i : fields) {
			total = total ^ i;
		}
		if (total == 0) {
			int row = 0;
			while (fields[row] == 0)
				row++;
			totalPiece--;
			fields[row]--;
		} else {
			int pos = 0;
			int copy = total;
			while (total != 0) {
				total = total >>> 1;
				pos++;
			}
			int mask = (int) Math.pow(2, pos - 1);
			for (int i = 0; i < rowNumber; i++) {
				if ((fields[i] & mask) != 0) {
					int newNum = fields[i] ^ copy;
					totalPiece -= fields[i] - newNum;
					fields[i] = newNum;
					break;
				}
			}
		}
		return 1;
	}

	void setNextMove(Window window) {
		nextMove = window.getFields(rowNumber);
		window.setTfields(rowNumber);
	}

	void turn(Window window) {
		int success = 0;
		if (nextMove != null)
			success = move(nextMove[0], nextMove[1]);
		if (success == 1) {
			if (gameOver())
				finish();
			else {
				player = 1;
				move();
				if (gameOver())
					finish();
				backup();
				player = 0;
			}
		}
	}

	void finish() {
		if (player == 1)
			Utils.error("Lose", "You lose!");
		else
			Utils.error("Win", "You win!");
	}

	boolean gameOver() {
		return totalPiece == 0;
	}
}