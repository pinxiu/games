package nim;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import javax.swing.event.*;

public class Utils {
	public static void error(String type, String message, String... args) {
		switch (type) {
			case "exit":
				Object[] options = {args[0], args[1]};
				int choice = JOptionPane.showOptionDialog(null, null, message,
				JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
				if (choice == JOptionPane.NO_OPTION)
					System.exit(0);
				break;
			case "Invalid":
				JOptionPane.showMessageDialog(null, message, "Failure", JOptionPane.PLAIN_MESSAGE);
				break;
			case "size":
				JOptionPane.showMessageDialog(null, message, "Warning", JOptionPane.PLAIN_MESSAGE);
				break;
			case "Lose":
				JOptionPane.showMessageDialog(null, message, "Game Over", JOptionPane.PLAIN_MESSAGE);
				break;
			case "Win":
				JOptionPane.showMessageDialog(null, message, "Game Over", JOptionPane.PLAIN_MESSAGE);
				break;
			case "help":
				JOptionPane.showMessageDialog(null, message, "Instruction", JOptionPane.PLAIN_MESSAGE);
				break;
			default: // Do nothing.
		}
		
	}
}