package nim;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import javax.swing.event.*;

import java.io.File;

public class Control extends JToolBar {
	private Action newGame = new NewGameAction("New");
	private Action undo = new UndoAction("Undo");
	private Action restart = new RestartAction("Restart");
	private Action image = new ImageAction("Image");
	private Action help = new HelpAction("Help");
	private JButton defaultButton = new JButton(newGame);
	private Window window;

	JButton getDefaultButton() {
		return defaultButton;
	}

	Control(Window window) {
		this.window = window;
		this.setLayout(new FlowLayout(FlowLayout.LEFT));
		this.setBackground(Color.WHITE);

		this.add(defaultButton);
		this.add(new JButton(undo));
		this.add(new JButton(restart));
		this.add(new JButton(image));
		this.add(new JButton(help));
		
		window.getPopup().add(new JMenuItem(newGame));
		window.getPopup().add(new JMenuItem(undo));
		window.getPopup().add(new JMenuItem(restart));
		window.getPopup().add(new JMenuItem(image));
		window.getPopup().add(new JMenuItem(help));
		
	}

	private class HelpAction extends AbstractAction {
		public HelpAction(String name) {
			super(name);
		}

		public void actionPerformed(ActionEvent e) {
			String text = "        Nim is a mathematical game of strategy in which two players take\n";
			text = text + "turns removing objects from distinct heaps. In this specific game, we\n";
			text = text + "cross out blocks from different rows. On each turn, a player must cross\n";
			text = text + "out at least one block, and may cross out any number of blocks provided\n";
			text = text + "they all come from the same row. The goal of the game is to be the player\n";
			text = text + "who crosses out the last object.\n";
			text = text + "        You can start a new game by pressing \"New\" button. Follow the\n";
			text = text + "prompts and you will see the blocks displayed on the screen. In this game,\n";
			text = text + "we only accept positive integers as the number of rows and the number of\n";
			text = text + "blocks in each row. We do accept large numbers like 1000, but you need to\n";
			text = text + "adjust the size of the screen in order to see all the blocks. You can move\n";
			text = text + "the window around as you like.\n";
			text = text + "        After you start a new game, you can start playing by entering the\n";
			text = text + "number of blocks you want to cross out in the corresponding text field.\n";
			text = text + "You can only cross out blocks in the same row on each turn. You can undo\n";
			text = text + "an action by pressing \"Undo\" button. You can also restart the game from\n";
			text = text + "the beginning, with the same settings as the current one.\n";
			text = text + "        You can change the background image by pressing \"Image\" button.\n";
			text = text + "You can either type in the path to the image file you want or restore the\n";
			text = text + "background image to default by typing \"default\". The path of an image\n";
			text = text + "file under the same directory as this program is just [imagename.format]\n";
			text = text + "e.g. bgpic.jpg . We do not support space in file names. The path to the\n";
			text = text + "parent directory of this game program is ../ .\n";
			text = text + "        To exit the game, you just need to close the window.";
			Utils.error("help", text);
		}
	}

	private class ImageAction extends AbstractAction {
		public ImageAction(String name) {
			super(name);
		}

		public void actionPerformed(ActionEvent e) {
			String filename = JOptionPane.showInputDialog("Please enter file name or \"default\".");
			if (filename != null) {
				if (filename.equals("default"))
					filename = "images/default_bg_pic.png";
				File newFile = new File(filename);
				if (!newFile.exists()) {
					Utils.error("Invalid", "Cannot find file");
				} else {
					window.setBImage(newFile);
				}
			}
		}
	}

	private class RestartAction extends AbstractAction {
		public RestartAction(String name) {
			super(name);
		}

		public void actionPerformed(ActionEvent e) {
			if (window.getGame() != null) {
				window.getGame().restart();
				Object[] options = {"I want to go first.", "Let the computer start."};
				int choice = JOptionPane.showOptionDialog(null, null, "Do you want to start or do you want the computer to start?",
				JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
				if (choice == JOptionPane.NO_OPTION)
					window.getGame().move();
				window.repaint();
				window.getGame().setHappen();
				window.getGame().backup();
				window.getGame().turn(window);
			}
		}
	}


	private class UndoAction extends AbstractAction {
		public UndoAction(String name) {
			super(name);
		}

		public void actionPerformed(ActionEvent e) {
			if (window.getGame() != null)
				window.getGame().undo();
			window.repaint();
		}
	}

	private class NewGameAction extends AbstractAction {
		public NewGameAction(String name) {
			super(name);
		}

		public void actionPerformed(ActionEvent e) {
 			String input = JOptionPane.showInputDialog("How many rows would you like?");
 			if (input != null)
 				try {
 					int rowNumber = Integer.parseInt(input);
 					if (rowNumber <= 0)
 						Utils.error("Invalid", "Invalid number.");
 					else {
 						JPanel p = new JPanel();
 						p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
 						JTextField[] nums = new JTextField[rowNumber];
 						for (int i = 0; i < rowNumber; i++) {
 							nums[i] = new JTextField(3);
 							p.add(new JLabel("Row " + Integer.toString(i + 1) + " :"));
 							p.add(nums[i]);
 						}
 						int option = JOptionPane.showConfirmDialog(null, p, "Number of blocks",
 									 JOptionPane.OK_CANCEL_OPTION);
 						if (option == JOptionPane.OK_OPTION) {
 							int[] fields = new int[rowNumber];
 							boolean success = true;
 							for (int i = 0; i < rowNumber; i++) {
 								fields[i] = Integer.parseInt(nums[i].getText());
 								if (fields[i] <= 0) {
 									Utils.error("Invalid", "Invalid number.");
 									success = false;
 									break;
 								}
 							}
 							if (success) {
 								window.setGame(new Game(rowNumber));
 								window.getGame().setFields(fields);
								window.setSize();
 								window.setBlockSize();
 								window.setTfields(rowNumber);
								window.repaint();
 								Object[] options = {"I want to go first.", "Let the computer start."};
								int choice = JOptionPane.showOptionDialog(null, null, "Do you want to start or do you want the computer to start?",
								JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
								if (choice == JOptionPane.NO_OPTION)
									window.getGame().move();
								window.repaint();
								window.getGame().setHappen();
								window.getGame().backup();
 								window.getGame().turn(window);
 							}
 						}
 					}
 				} catch (Exception ex) {
 					Utils.error("Invalid", "Invalid number.");
 				}
		}
	}
}